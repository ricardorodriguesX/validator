/**
 * 
 * @returns {Validator}
 */
function Validator() {}


/**
 * 
 * @param {type} valor
 * @returns {MessageValidator}
 */
Validator.validaPreenchido = function (valor) {
    if (valor === undefined || valor === "" || valor === null) {
        return new MessageValidator(false, "Valor não preenchido");
    } else {
        return new MessageValidator(true, "Valor preenchido");
    }
};


/**
 * 
 * @param {type} valor
 * @param {type} tamanhoMinimo
 * @param {type} tamanhoMaximo
 * @returns {MessageValidator}
 */
Validator.validaAlfaNumericoHumano = function (valor, tamanhoMinimo, tamanhoMaximo) {
    valor = valor.toString();
    if (!/\W/g.test(valor)) {
        return new MessageValidator(false, "Valor com caracteres inválidos");
    } else if (!this.validaComprimento(valor.length, tamanhoMinimo).getBooleano()) {
        return new MessageValidator(false, "Valor menor que " + tamanhoMinimo + " caracteres");
    } else if (!this.validaComprimento(valor.length, null, tamanhoMaximo).getBooleano()) {
        return new MessageValidator(false, "Valor maior que " + tamanhoMinimo + " caracteres");
    } else {
        return new MessageValidator(true, "Valor válido");
    }
};


/**
 * 
 * @param {type} valor
 * @param {type} minimo
 * @param {type} maximo
 * @returns {MessageValidator}
 */
Validator.validaComprimento = function (valor, minimo, maximo) {
    if ((minimo !== undefined && minimo !== null) && valor < minimo) {
        return new MessageValidator(false, "Valor menor que minimo");
    } else if ((maximo !== undefined && maximo !== null) && valor > maximo) {
        return new MessageValidator(false, "Valor maior que maximo");
    } else {
        return new MessageValidator(true, "Comprimento válido");
    }
};


/**
 * 
 * @param {type} numero
 * @param {type} numeroMinimo
 * @param {type} numeroMaximo
 * @returns {MessageValidator}
 */
Validator.validaNumero = function (numero, numeroMinimo, numeroMaximo) {
    if (!this.validaPreenchido(numero).getBooleano()) {
        return new MessageValidator(false, "Numero não preenchido");
    } else if (isNaN(numero)) {
        return new MessageValidator(false, "Não é numero");
    } else if (!this.validaComprimento(numero, numeroMinimo).getBooleano()) {
        return new MessageValidator(false, "Numero menor que minimo");
    } else if (!this.validaComprimento(numero, null, numeroMaximo).getBooleano()) {
        return new MessageValidator(false, "Numero maior que maximo");
    } else {
        return new MessageValidator(true, "Numero válido");
    }
};


/**
 * 
 * @param {type} texto
 * @param {type} tamanhoMinimo
 * @param {type} tamanhoMaximo
 * @returns {MessageValidator}
 */
Validator.validaTexto = function (texto, tamanhoMinimo, tamanhoMaximo) {
    if (!this.validaPreenchido(texto).getBooleano()) {
        return new MessageValidator(false, "Texto não preenchido");
    } else if (/\d/.test(texto)) {
        return new MessageValidator(false, "Texto contém números");
    } else if (!this.validaComprimento(texto.length, tamanhoMinimo).getBooleano()) {
        return new MessageValidator(false, "Texto menor que minimo");
    } else if (!this.validaComprimento(texto.length, null, tamanhoMaximo).getBooleano()) {
        return new MessageValidator(false, "Texto maior que maximo");
    } else {
        return new MessageValidator(true, "Texto válido");
    }
};


/**
 * 
 * @param {type} nif
 * @param {type} ePT
 * @param {type} tamanhoMinimo
 * @param {type} tamanhoMaximo
 * @returns {MessageValidator}
 */
Validator.validaNIF = function (nif, ePT, tamanhoMinimo, tamanhoMaximo) {
    nif = nif.toString();

    if (!this.validaPreenchido(nif).getBooleano()) {
        return new MessageValidator(false, "NIF não preenchido");
    } else if (ePT) {
        if (!this.validaNumero(nif).getBooleano()) {
            return new MessageValidator(false, "NIF não é numero");
        } else if (!this.validaComprimento(nif.length, 9, 9).getBooleano()) {
            return new MessageValidator(false, "NIF não tem 9 numeros");
        } else {
            while (nif.length < 9) {
                nif = "0" + nif;
            }

            var arraycaracteres = nif.split("").map(Number);
            var calc = 9 * arraycaracteres[0] + 8 * arraycaracteres[1] + 7 * arraycaracteres[2] + 6 * arraycaracteres[3] + 5 * arraycaracteres[4] + 4 * arraycaracteres[5] + 3 * arraycaracteres[6] + 2 * arraycaracteres[7] + arraycaracteres[8];
            if ((calc % 11 === 0) || (arraycaracteres[8] === 0 && (calc + 10) % 11 === 0)) {
                return new MessageValidator(true, "NIF válido");
            } else {
                return new MessageValidator(false, "NIF é inválido");
            }
        }
    } else if (!this.validaComprimento(nif.length, tamanhoMinimo).getBooleano()) {
        return new MessageValidator(false, "NIF é menor que o minimo");
    } else if (!this.validaComprimento(nif.length, null, tamanhoMaximo).getBooleano()) {
        return new MessageValidator(false, "NIF é maior que o maximo");
    } else {
        return new MessageValidator(true, "NIF válido");
    }
};


/**
 * 
 * @param {type} cartaoCidadao
 * @param {type} ePT
 * @param {type} digitoCheck
 * @param {type} tamanhoMinimo
 * @param {type} tamanhoMaximo
 * @returns {MessageValidator}
 */
Validator.validaCartaoCidadao = function (cartaoCidadao, ePT, digitoCheck, tamanhoMinimo, tamanhoMaximo) {
    cartaoCidadao = cartaoCidadao.toString();
    if (!this.validaPreenchido(cartaoCidadao).getBooleano()) {
        return new MessageValidator(false, "Cartão de Cidadão não preenchido");
    } else if (ePT) {
        if (!this.validaNumero(cartaoCidadao).getBooleano()) {
            return new MessageValidator(false, "Cartão de Cidadão não é numero");
        } else if (!this.validaComprimento(cartaoCidadao.length, 7).getBooleano()) {
            return new MessageValidator(false, "Cartão de Cidadão é menor que 7 numeros");
        } else {
            var sum = 0;
            var arrayCaracteres = cartaoCidadao.split("").map(Number);

            for (var i = 2; i < arrayCaracteres.length; i++) {
                sum += (arrayCaracteres[i] - '0') * i;
            }

            if (sum % 11 !== digitoCheck) {
                return new MessageValidator(false, "Cartão de Cidadão Inválido");
            } else {
                return new MessageValidator(true, "Cartão de Cidadão válido");
            }

        }
    } else if (!this.validaComprimento(cartaoCidadao.length, tamanhoMinimo).getBooleano()) {
        return new MessageValidator(false, "Cartão de Cidadão é menor que o minimo");
    } else if (!this.validaComprimento(cartaoCidadao.length, null, tamanhoMaximo).getBooleano()) {
        return new MessageValidator(false, "Cartão de Cidadão é maior que o maximo");
    } else {
        return new MessageValidator(true, "Cartão de Cidadão válido");
    }
};


/**
 * 
 * @param {type} data
 * @param {type} dataMinima
 * @param {type} dataMaxima
 * @returns {MessageValidator}
 */
Validator.validaData = function (data, dataMinima, dataMaxima) {
    if (!this.validaPreenchido(data).getBooleano()) {
        return new MessageValidator(false, "Data não preenchida");
    } else {
        try {
            data = new Date(data);
            dataMinima = new Date(dataMinima);
            dataMaxima = new Date(dataMaxima);

            if (!this.validaNumero(data.getDate()).getBooleano()) {
                return new MessageValidator(false, "Data inválida");
            } else if (data < dataMinima) {
                return new MessageValidator(false, "Data menor que a data minima");
            } else if (data > dataMaxima) {
                return new MessageValidator(false, "Data maior que a data maxima");
            } else {
                return new MessageValidator(true, "Data válida");
            }
        } catch (e) {
            return new MessageValidator(false, "Data inválida");
        }
    }
};


/**
 * 
 * @param {type} data
 * @param {type} idadeMinima
 * @param {type} idadeMaxima
 * @returns {MessageValidator}
 */
Validator.validaDataNascimento = function (data, idadeMinima, idadeMaxima) {
    if (!this.validaPreenchido(data).getBooleano()) {
        return new MessageValidator(false, "Data de Nascimento não preenchida");
    } else if (!this.validaData(data).getBooleano()) {
        return new MessageValidator(false, "Data inválida");
    } else {
        data = new Date(data);
        var dataHoje = new Date();

        if (!this.validaComprimento(dataHoje.getFullYear() - data.getFullYear(), idadeMinima).getBooleano()) {
            return new MessageValidator(false, "Idade com menos de " + idadeMinima + " anos");
        } else if (!this.validaComprimento(dataHoje.getFullYear() - data.getFullYear(), null, idadeMaxima).getBooleano()) {
            return new MessageValidator(false, "Idade com mais de " + idadeMaxima + " anos");
        } else {
            return new MessageValidator(true, "Data válida");
        }
    }
};


/**
 * 
 * @param {type} morada
 * @param {type} tamanhoMaximo
 * @returns {MessageValidator}
 */
Validator.validaMorada = function (morada, tamanhoMaximo) {
    if (!this.validaPreenchido(morada).getBooleano()) {
        return new MessageValidator(false, "Morada não preenchida");
    } else if (!this.validaComprimento(morada.length, null, tamanhoMaximo).getBooleano()) {
        return new MessageValidator(false, "Morada maior que o tamanho máximo");
    } else {
        return new MessageValidator(true, "Morada válida");
    }
};


/**
 * 
 * @param {type} localidade
 * @param {type} tamanhoMaximo
 * @returns {MessageValidator}
 */
Validator.validaLocalidade = function (localidade, tamanhoMaximo) {
    if (!this.validaPreenchido(localidade).getBooleano()) {
        return new MessageValidator(false, "Localidade não preenchida");
    } else if (!this.validaComprimento(localidade.length, null, tamanhoMaximo).getBooleano()) {
        return new MessageValidator(false, "Localidade maior que o tamanho máximo");
    } else if (!this.validaTexto(localidade).getBooleano()) {
        return new MessageValidator(false, "Localidade com numeros");
    } else {
        return new MessageValidator(true, "Localidade válida");
    }
};

/**
 * 
 * @param {type} codigoPostal
 * @param {type} ePT
 * @param {type} tamanhoMinimo
 * @param {type} tamanhoMaximo
 * @returns {MessageValidator}
 */
Validator.validaCodigoPostal = function (codigoPostal, ePT, tamanhoMinimo, tamanhoMaximo) {
    codigoPostal = codigoPostal.toString();
    if (!this.validaPreenchido(codigoPostal).getBooleano()) {
        return new MessageValidator(false, "Código Postal não preenchido");
    } else if (ePT) {
        if (codigoPostal.indexOf("-") < 0) {
            return new MessageValidator(false, "Código Postal com formato inválido");
        } else {
            codigoPostal = codigoPostal.split("-");

            if (!this.validaNumero(codigoPostal[0]).getBooleano() || !this.validaNumero(codigoPostal[1]).getBooleano()) {
                return new MessageValidator(false, "Código Postal com letras");
            } else if (!this.validaComprimento(codigoPostal[0].length, 4, 4).getBooleano() || !this.validaComprimento(codigoPostal[1].length, 3, 3).getBooleano()) {
                return new MessageValidator(false, "Código Postal com formato inválido");
            } else {
                return new MessageValidator(true, "Código Postal válido");
            }
        }
    } else {
        if (!this.validaComprimento(codigoPostal.length, tamanhoMinimo).getBooleano()) {
            return new MessageValidator(false, "Código Postal menor que o tamanho minimo");
        } else if (!this.validaComprimento(codigoPostal.length, null, tamanhoMaximo).getBooleano()) {
            return new MessageValidator(false, "Código Postal maior que o tamanho máximo");
        } else {
            return new MessageValidator(true, "Código Postal válido");
        }
    }
};


/**
 * 
 * @param {type} pais
 * @param {type} arrayPaises
 * @returns {MessageValidator}
 */
Validator.validaPais = function (pais, arrayPaises) {
    if (!this.validaPreenchido(pais).getBooleano()) {
        return new MessageValidator(false, "País não preenchido");
    } else {
        for (var i = 0; i < arrayPaises; i++) {
            if (pais === arrayPaises[i]) {
                return new MessageValidator(true, "País válido");
            }
        }

        return new MessageValidator(true, "País inválido");
    }
};


/**
 * 
 * @param {type} distrito
 * @param {type} ePT
 * @param {type} arrayDistritos
 * @returns {MessageValidator}
 */
Validator.validaDistrito = function (distrito, ePT, arrayDistritos) {
    if (!this.validaPreenchido(distrito).getBooleano()) {
        return new MessageValidator(false, "Distrito não preenchido");
    } else if (ePT) {
        for (var i = 0; i < arrayDistritos; i++) {
            if (distrito === arrayDistritos[i]) {
                return new MessageValidator(true, "Distrito válido");
            }
        }

        return new MessageValidator(true, "Distrito inválido");
    } else {
        if (!this.validaTexto(distrito).getBooleano()) {
            return new MessageValidator(true, "Distrito com numeros");
        } else {
            return new MessageValidator(true, "Distrito válido");
        }
    }
};


/**
 * 
 * @param {type} telemovel
 * @param {type} ePT
 * @param {type} tamanhoMinimo
 * @param {type} tamanhoMaximo
 * @returns {MessageValidator}
 */
Validator.validaTelemovel = function (telemovel, ePT, tamanhoMinimo, tamanhoMaximo) {
    telemovel = telemovel.toString();
    if (!this.validaPreenchido(telemovel).getBooleano()) {
        return new MessageValidator(false, "Telemovel não preenchido");
    } else if (ePT) {
        if (!this.validaNumero(telemovel).getBooleano()) {
            return new MessageValidator(false, "Telemovel com letras");
        } else if (!this.validaComprimento(telemovel.length, 9, 9).getBooleano()) {
            return new MessageValidator(false, "Telemovel não tem 9 numeros");
        } else {
            return new MessageValidator(true, "Telemovel válido");
        }
    } else {
        if (telemovel.indexOf("+") !== 0) {
            return new MessageValidator(false, "Telemovel com formato inválido");
        } else if (!this.validaComprimento(telemovel.length, tamanhoMinimo).getBooleano()) {
            return new MessageValidator(false, "Telemovel menor que " + tamanhoMinimo + " numeros");
        } else if (!this.validaComprimento(telemovel.length, null, tamanhoMaximo).getBooleano()) {
            return new MessageValidator(false, "Telemovel maior que " + tamanhoMaximo + " numeros");
        } else {
            return new MessageValidator(true, "Telemovel válido");
        }
    }
};


/**
 * 
 * @param {type} telefone
 * @param {type} ePT
 * @param {type} tamanhoMinimo
 * @param {type} tamanhoMaximo
 * @returns {MessageValidator}
 */
Validator.validaTelefone = function (telefone, ePT, tamanhoMinimo, tamanhoMaximo) {
    telefone = telefone.toString();
    if (!this.validaPreenchido(telefone).getBooleano()) {
        return new MessageValidator(false, "Telefone não preenchido");
    } else if (ePT) {
        if (!this.validaNumero(telefone).getBooleano()) {
            return new MessageValidator(false, "Telefone com letras");
        } else if (!this.validaComprimento(telefone.length, 9, 9)) {
            return new MessageValidator(false, "Telefone não tem 9 numeros");
        } else {
            return new MessageValidator(true, "Telefone válido");
        }
    } else {
        if (telefone.indexOf("+") !== 0) {
            return new MessageValidator(false, "Telefone com formato inválido");
        } else if (!this.validaComprimento(telefone.length, tamanhoMinimo).getBooleano()) {
            return new MessageValidator(false, "Telefone menor que " + tamanhoMinimo + " numeros");
        } else if (!this.validaComprimento(telefone.length, null, tamanhoMaximo).getBooleano()) {
            return new MessageValidator(false, "Telefone maior que " + tamanhoMaximo + " numeros");
        } else {
            return new MessageValidator(true, "Telefone válido");
        }
    }
};


/**
 * 
 * @param {type} email
 * @param {type} tamanhoMaximo
 * @returns {MessageValidator}
 */
Validator.validaEmail = function (email, tamanhoMaximo) {
    if (!this.validaPreenchido(email).getBooleano()) {
        return new MessageValidator(false, "Email não preenchido");
    } else if (!this.validaComprimento(email.length, null, tamanhoMaximo).getBooleano()) {
        return new MessageValidator(false, "Email maior que " + tamanhoMaximo + " caracteres");
    } else {
        var posAt = email.indexOf("@");
        var posPonto = email.lastIndexOf(".");

        if (posAt < 1 || posPonto < posAt + 2 || posPonto + 2 > email.length) {
            return new MessageValidator(false, "Email com formato inválido");
        } else {
            return new MessageValidator(true, "Email válido");
        }
    }
};


/**
 * 
 * @param {type} username
 * @param {type} tamanhoMaximo
 * @returns {MessageValidator}
 */
Validator.validaUsername = function (username, tamanhoMaximo) {
    if (!this.validaPreenchido(username).getBooleano()) {
        return new MessageValidator(false, "Username não preenchido");
    } else if (!this.validaAlfaNumericoHumano(username).getBooleano()) {
        return new MessageValidator(false, "Username com caracteres inválidos");
    } else if (!this.validaComprimento(username.length, null, tamanhoMaximo).getBooleano()) {
        return new MessageValidator(false, "Username maior que " + tamanhoMaximo + " caracteres");
    } else {
        return new MessageValidator(true, "Username válido");
    }
};


/**
 * 
 * @param {type} password
 * @param {type} confirmacao
 * @param {type} tamanhoMinimo
 * @param {type} tamanhoMaximo
 * @returns {MessageValidator}
 */
Validator.validaPassowrds = function (password, confirmacao, tamanhoMinimo, tamanhoMaximo) {
    if (!this.validaPreenchido(password).getBooleano()) {
        return new MessageValidator(false, "Password não preenchida");
    } else if (!this.validaPreenchido(confirmacao).getBooleano()) {
        return new MessageValidator(false, "Confirmação de Password não preenchida");
    }else if(!this.validaComprimento(password.length, tamanhoMinimo).getBooleano()){
        return new MessageValidator(false, "Password menor que " + tamanhoMinimo + " caracteres");
    }else if(!this.validaComprimento(confirmacao.length, tamanhoMinimo).getBooleano()){
        return new MessageValidator(false, "Confirmação de Password menor que " + tamanhoMinimo + " caracteres");
    }else if(!this.validaComprimento(password.length, null, tamanhoMaximo).getBooleano()){
        return new MessageValidator(false, "Password maior que " + tamanhoMaximo + " caracteres");
    }else if(!this.validaComprimento(confirmacao.length, null, tamanhoMaximo).getBooleano()){
        return new MessageValidator(false, "Confirmação de Password maior que " + tamanhoMaximo + " caracteres");
    }else if(password !== confirmacao){
        return new MessageValidator(false, "Password e Confirmação de passwords não coincidem");
    }else{
        return new MessageValidator(true, "Passwords válidas");
    }
};

/**
 * 
 * @param {type} nome
 * @returns {MessageValidator}
 */
Validator.validaNome = function (nome) {
    if (!this.validaPreenchido(nome).getValido()) {
        return new MessageValidator(false, "Nome não preenchido");
    } else if (!this.validaTexto(nome)) {
        return new MessageValidator(false, "Nome inválido");
    } else {
        return new MessageValidator(true, "Nome válido");
    }
};

/**
 * 
 * @param {type} texto
 * @param {type} tamanhoMinimo
 * @param {type} tamanhoMaximo
 * @returns {MessageValidator}
 */
Validator.validaDescricao = function (texto, tamanhoMinimo, tamanhoMaximo) {
    if (!this.validaPreenchido(texto).getValido()) {
        return new MessageValidator(false, "Descrição não preenchida");
    } else if (!this.validaComprimento(texto.length, tamanhoMinimo).getValido()) {
        return new MessageValidator(false, "Descrição menor que minimo");
    } else if (!this.validaComprimento(texto.length, null, tamanhoMaximo).getValido()) {
        return new MessageValidator(false, "Descrição maior que maximo");
    } else {
        return new MessageValidator(true, "Descrição válida");
    }
};

/**
 * 
 * @param {type} ficheiro
 * @param {type} ext
 * @param {type} size
 * @returns {MessageValidator}
 */
Validator.validaFicheiro = function(ficheiro, size){
    if(ficheiro.size > size){
        return new MessageValidator(false, "O tamanho do ficheiro ultrapassa o máximo " + (size/1048576).toFixed(2) + " MB");
    }else{
        return new MessageValidator(true, "Ficheiro associado.");
    }
};

/**
 * 
 * @param {type} hora
 * @returns {MessageValidator}
 */
Validator.validaHora = function(hora){
    hora.split(":");
    hora = new Date();
    hora.setHours(hora[0]);
    hora.setMinutes(hora[1]);
    
   if(!this.validaPreenchido(hora).getValido()){
        return new MessageValidator(false, "Hora inválida");
    }else{
        return new MessageValidator(true, "Hora válida");
    }
};