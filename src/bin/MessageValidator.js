/**
 * 
 * @param {type} valido
 * @param {type} mensagem
 * @returns {MessageValidator}
 */
function MessageValidator(valido, mensagem){
    this.valido = valido;
    this.mensagem = mensagem;
};

MessageValidator.prototype.getMensagem = function(){
    return this.mensagem;
};

MessageValidator.prototype.getValido = function(){
    return this.valido;
};