# VALIDATOR
Validações Genéricas e Portuguesas de Javascript para facilitar o desenvolvimento e validações de Front-End

# USO
```
<script>
    $(document).ready(function(){
      //EXEMPLO
      var validacao = Validator.validaTelemovel("123456789", true);
      
      if(!validacao.getValido()){
        $("div").text(validacao.getMensagem());
      }
     });
</script>
```
# FUNCOES
```
//valida se o valor esta preenchido
Validator.validaPreenchido(valor)
```
```
//valida se o valor tem apenas letras e numeros (subconjunto alfanumerico)
Validator.validaAlfaNumericoHumano(valor, tamanhoMinimo, tamanhoMaximo)
```
```
//valida se o valor esta compreendido entre o minimo e maximo
Validator.validaComprimento(valor, minimo, maximo)
```
```
//valida se o valor e um numero e se esta compreendido entre o minimo e maximo
//numeroMinimo, numeroMaximo sao parametros opcionais, se estes parametros nao forem passados,
//apenas e validado se e numero
Validator.validaNumero(numero, numeroMinimo, numeroMaximo)
```
```
//valida se o valor e texto sem numeros e o seu comprimento esta compreendido entre o minimo e maximo
//tamanhoMinimo, tamanhoMaximo sao parametros opcionais, se estes parametros nao forem passados,
//apenas e validado se e texto
Validator.validaTexto(texto, tamanhoMinimo, tamanhoMaximo)
```
```
//valida se o valor e um NIF valido
//se ePT for passado como true, o NIF sera validado conforme a norma portuguesa
//se nao e ePT apenas valida o seu comprimento
//tamanhoMinimo, tamanhoMaximo sao parametros opcionais so seram usados em caso de NIF nao PT
Validator.validaNIF(nif, ePT, tamanhoMinimo, tamanhoMaximo)
```
```
//valida se o valor e um CC valido
//se ePT for passado como true, o CC sera validado conforme a norma portuguesa com o digitoCheck
//se nao e ePT apenas valida o seu comprimento
//tamanhoMinimo, tamanhoMaximo sao parametros opcionais so seram usados em caso de CC nao PT
Validator.validaCartaoCidadao(cartaoCidadao, ePT, digitoCheck, tamanhoMinimo, tamanhoMaximo)
```
```
//valida se o valor e data valida no formato yyyy-mm-dd compreendida entre um minimo e maximo
//dataMinima, dataMaxima sao parametros opcionais, se estes parametros nao forem passados,
//apenas e validado se e data valida
Validator.validaData(data, dataMinima, dataMaxima)
```
```
//valida se o valor e uma data valida e se e uma idade compreendidade entre um minimo e maximo
Validator.validaDataNascimento(data, idadeMinima, idadeMaxima)
```
```
//valida se o valor esta preenchido e o seu comprimento nao ultrapassa o seu maximo
Validator.validaMorada(morada, tamanhoMaximo)
```
```
//valida se o valor e texto valido e o seu comprimento nao ultrapassa o seu maximo
Validator.validaLocalidade(localidade, tamanhoMaximo)
```
```
//valida se o valor e codigo postal valido na norma portuguesa nnnn-nnn caso o ePT seja true
//se nao e pt, valida se o seu comprimento esta compreendido entre tamanhoMinimo e o tamanhoMaximo
Validator.validaCodigoPostal(codigoPostal, ePT, tamanhoMinimo, tamanhoMaximo)
```
```
//valida se o pais existe num array de paises
Validator.validaPais(pais, arrayPaises)
```
```
//valida se o distrito existe num array de distritos se o ePT for true
//se nao e pt, valida se e texto valido
Validator.validaDistrito(distrito, ePT, arrayDistritos)
```
```
//valida se e numero de telemovel valido na norma portuguesa caso ePT for true
//se nao e pt, valida se o numero de telemovel tem indicativo + e se o seu comprimento esta entre tamanhoMinimo e tamanhoMaximo
Validator.validaTelemovel(telemovel, ePT, tamanhoMinimo, tamanhoMaximo)
```
```
//valida se e numero de telefone valido na norma portuguesa caso ePT for true
//se nao e pt, valida se o numero de telefone tem indicativo + e se o seu comprimento esta entre tamanhoMinimo e tamanhoMaximo
Validator.validaTelefone(telefone, ePT, tamanhoMinimo, tamanhoMaximo)
```
```
//valida se o valor e um email valido e se o seu comprimento esta nao passa o tamanhoMaximo
Validator.validaEmail(email, tamanhoMaximo)
```
```
//valida se o valor e alfanumerico e se o seu comprimento esta nao passa o tamanhoMaximo
Validator.validaUsername(username, tamanhoMaximo)
```
```
//valida se as passwords coincidem uma com a outra e se os seus comprimentos estao compreendidos entre tamanhoMinimo e tamanhoMaximo
Validator.validaPasswords(password, confirmacao, tamanhoMinimo, tamanhoMaximo)
```
```
//valida se o nome não contém caracteres inválidos e se está preenchido
Validator.validaNome(nome)
```
```
//valida se uma descrição está preenchida e se os seus comprimentos estao compreendidos entre tamanhoMinimo e tamanhoMaximo
Validator.validaDescricao(texto, tamanhoMinimo, tamanhoMaximo)
```
```
//valida se o tamanho de um ficheiro não ultrapassa o máximo de MB
Validator.validaFicheiro(ficheiro, size)
```
```
//valida se a hora tem o formato correto h:i
Validator.validaHora(hora)
```
# COMO ADICIONAR NOVAS FUNCOES
O utilizador pode adicionar novas funções de validação específicas ao projeto seguindo o exempo indicado.

Exemplo:
```
Validator.nomeValidacao = function(param1, param2, paramN){
     // VALIDACOES A FAZER
}
```
